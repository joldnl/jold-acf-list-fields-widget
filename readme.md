# Introduction
**ACF Fields Overview Widget** Plugin adds a Dashboard Widget with a complete overview of all registered field groups and fields trough the Advanced Custom Fields Pro plugin.
This is especially handy to get a birdseye overview of all of your custom fields' names, types and labels.

### Main Features
- Add a dashboard widget with all Advanced Custom Fields Metaboxes / Groups and fields
- Only visible for users with Administrator capabilities
- List conditions where to show the acf metabox / group.
- List all ACF Field Metaboxes / Groups
  - List all fields
  - List sub fields of repeater field
  - List flexible content sub fields

### Dependancies
This plugin needs some dependancies to work properly. The following dependancies are to be installed. All dependencies are managed with composer and gulp!. This is only necessary if there are made changes to the asset files (.scss) in the assets folder.
- nodejs
- gulp

### Install
Install the ACF Fields Overview Widget plugin with composer by incorporating the following code into your composer.json file.

**1. Add the package repository to your composer.json file**

    {
        "type": "vcs",
        "url": "git@bitbucket.org:joldnl/jold-acf-list-fields-widget.git"
    },

**2. Add the plugin as a dependency to your composer.json file**

    "joldnl/jold-acf-list-fields-widget": "master",


**3. Run composer update**

    $ composer update
