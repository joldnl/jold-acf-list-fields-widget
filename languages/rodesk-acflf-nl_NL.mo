��    2      �  C   <      H     I  �   N  
   �  	   �     �     �               &  O   4     �     �     �  
   �     �     �     �  	   �     �  &   �     �  &        6     C     L     Q     ]  	   k     u     �     �     �     �     �     �  	   �  
   �     �  	   �  
   �     	          2     8  	   =  	   G     Q     b     t  �  {     #
  c   '
     �
     �
     �
     �
     �
     �
     �
  =   �
     2     :     P  
   ]  
   h     s     {     �     �     �     �  >   �                    #     8     H  *   T          �     �     �     �     �     �     �          	          $     3  
   H     S     X     m     {     �     �                   1                                           2       $       +   .      )                     -      ,                      
             #       %       0   	   (       "          '            !          *   &   /          AND: Advanced Custom Fields Pro plugin does not seem to be active. Please <a href='plugins.php'>activate</a> Advanced Custom Fields Pro! Attachment Auto Save Child Page (has parent) Comment Current user Current user role Custom Fields Display a list of all registered Advanced Custom Field Groups and their fields. Draft Edit field group Field Groups Front Page Future Inherit Label Logged in Name No Advanced Custom Field groups found. No fields found. No options page exists. None selected. Options page Or when: Page Page parent Page template Page type Parent Page (has children) Pendig review Post Post category Post format Post status Post taxonomy Post type Posts Page Private Published Show when: Taxonomy Term Top Level Page (no parent) Trash Type User form User role Viewing back end Viewing front end Widget Project-Id-Version: ACF: Dashboard Fields Widget
POT-Creation-Date: 2016-04-18 15:47+0200
PO-Revision-Date: 2016-04-18 15:51+0200
Last-Translator: 
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
X-Poedit-WPHeader: acf-list-fields.php
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 EN: ACF Pro plugin niet lijkt actief te zijn. <a href=‘plugins.php’>Activeer</a> de ACF Pro plugin! Bijlage Auto. opgeslagen Dochterpagina Reactie Huidige gebruiker Huidige gebruikersrol Aangepaste velden Toon een lijst met alle geregistreerde ACF groepen en velden. Concept Velden groep bewerken Veld groepen Voorpagina Toekomstig Geërfd Label Ingelogd Naam Geen ACF veld groepen gevonden. Geen velden gevonden. Er zijn geen optiepagina’s geregistreerd. Geen geselecteerd. Optiepagina Of wanneer: Pagina Bovenliggende pagina Pagina sjabloon Pagina type Bovenliggende pagina (heeft dochterpagina) Afwachting van beoordeling Bericht Bericht categorie Bericht format Bericht status Bericht taxonomie Bericht type Berichten pagina Privé Gepubliceerd Toon wanneer: Taxonomie Term Bovenliggende pagina Verwijderd Type Gebruikers formulier Gebruikersrol Front end bekijken Admin bekijken Widget 