<?php
/*
Plugin Name:    ACF: Dashboard Fields Widget
Plugin URI:     https://bitbucket.org/oldnl/jold-acf-list-fields-widget
Description:    Adds a dashboard widget with all registered advanced custom field groups and fields. This is handy for quick overview of all fields and field names
Version:        0.1.9
Author:         Jurgen Oldenburg
Author URI:     http://www.jold.nl
Text Domain:    jld-acflf
Domain Path:    /languages
License:        GPL-3.0
License URI:    https://www.gnu.org/licenses/gpl-3.0.html
*/


define( "ACFLF_PLUGIN_FOLDER", dirname( __FILE__ ) );
define( "ACFLF_PLUGIN_DIR", plugins_url( '', __FILE__ ) );
define( "ACFLF_PLUGIN_NAME", "acf_list_fields" );
define( "ACFLF_PLUGIN_TEXTDOMAIN", "jld-acflf" );
define( "ACFLF_PLUGIN_DBPREFIX", "acflf_" );
define( "ACFLF_PLUGIN_VERSION", "0.1.5" );


new JoldAcfOverviewWidget;
class JoldAcfOverviewWidget {

    /**
    * Construct to fire functions on load
    */
    function __construct() {
        add_action( 'wp_dashboard_setup', array( $this, 'register_dashboard_widget' ) );
        add_action( 'wp_dashboard_setup', array( $this, 'load_assets') );
        add_action( 'plugins_loaded', array( $this, 'textdomain' ) );
    }




    /**
    * Add plugin textdomain translations
    * @return none
    */
    function textdomain() {
        load_plugin_textdomain( 'rodesk-acflf', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
    }




    /**
    * Check if widget should be showd for current user
    * @return  boolean   return true or false
    */
    function allow_user() {
        if( current_user_can("manage_options") ) {
            return true;
        }
    }



    /**
    * Register the dashboard widget
    * @return none
    */
    function register_dashboard_widget() {
        if( $this->allow_user() === true ){
            wp_add_dashboard_widget(
                'acflf',                                    // Widget slug.
                get_bloginfo( 'name' ) . ': ACF Overview',    // Title.
                array( $this, 'acflf_display_widget' )      // Display function.
            );
        }
    }



    /**
    * Get the content of the widget
    * @return none
    */
    function acflf_display_widget() {
        include( ACFLF_PLUGIN_FOLDER . "/parts/widget-content.php" );
    }



    /**
    * Register and load assets on plugin pages
    * @param  string $hook WordPress Hook
    * @return none
    */
    function load_assets( $hook ) {

        $screen = get_current_screen();    // Get Information about the current admin screen
        if( $screen->id === "dashboard" && $this->allow_user() === true ) {    // Only load scripts and styles on the dashboard page

            // Load style plugin styles
            wp_register_style( 'acflf-main', ACFLF_PLUGIN_DIR . '/dist/css/styles.min.css', '', ACFLF_PLUGIN_VERSION, 'all' );
            wp_enqueue_style( 'acflf-main' );

            // Load style plugin styles
            wp_register_style( 'acflf-print', ACFLF_PLUGIN_DIR . '/dist/css/styles-print.min.css', '', ACFLF_PLUGIN_VERSION, 'print' );
            wp_enqueue_style( 'acflf-print' );

        }
    }



    /**
    * Format an operator to human readable text
    * @param  [operator] $operator
    * @return [text]     "Is" or "Is not"
    */
    function clean_operator( $operator ) {
        if ( $operator === "==" ) {
            return "is";
        } else if ( $operator === "!=" ) {
            return "is not";
        } else {
            return $operator;
        }
    }



    /**
    * Format an selector to nice text
    * @param  [operator] $selector
    * @return [text]     Nicely formatted selector
    */
    function clean_selector( $selector ) {
        $labels = array(
            "post_type" => __("Post type", 'jld-acflf'),
            "post_status" => __("Post status", 'jld-acflf'),
            "post_category" => __("Post category", 'jld-acflf'),
            "post_taxonomy" => __("Post taxonomy", 'jld-acflf'),
            "post_format" => __("Post format", 'jld-acflf'),
            "post" => __("Post", 'jld-acflf'),

            "page_template" => __("Page template", 'jld-acflf'),
            "page_type" => __("Page type", 'jld-acflf'),
            "page_parent" => __("Page parent", 'jld-acflf'),
            "page" => __("Page", 'jld-acflf'),

            "current_user" => __("Current user", 'jld-acflf'),
            "current_user_role" => __("Current user role", 'jld-acflf'),
            "user_form" => __("User form", 'jld-acflf'),
            "user_role" => __("User role", 'jld-acflf'),

            "attachment" => __("Attachment", 'jld-acflf'),
            "taxonomy" => __("Taxonomy Term", 'jld-acflf'),
            "comment" => __("Comment", 'jld-acflf'),
            "widget" => __("Widget", 'jld-acflf'),
            "options_page" => __("Options page", 'jld-acflf')
        );

        return $labels[$selector];
    }



    /**
    * Get the ACF group/metabox location rules
    * @param  [string]   $rule_groups    Group to look for
    * @return [array]                    Location rules
    */
    function get_group_locations( $rule_groups ) {

        $rule_groups_i = 1;
        $locations = array();
        $locations["locations"] = array();

        foreach ( $rule_groups['location'] as $rule_group ) {

            $j = 1;
            $total_groups = count($rule_group);

            if( $rule_groups_i === 1 ){
                $label = __("Show when:", 'jld-acflf');
            } else {
                $label = __("Or when:", 'jld-acflf');
            }

            $temp_locations = array();
            $temp_locations["label"] = array();
            $temp_locations["params"] = array();
            $temp_locations["label"] = $label;

            foreach ( $rule_group as $location ) {

                if( $j === $total_groups ) {
                    $loc = array(
                        "param" => $this->clean_selector($location["param"]),
                        "operator" => $this->clean_operator($location["operator"]),
                        "value" => $location["value"],
                        "append" => ""
                    );
                } else {
                    $loc = array(
                        "param" => $this->clean_selector($location["param"]),
                        "operator" => $this->clean_operator($location["operator"]),
                        "value" => $location["value"],
                        "append" => __("AND:", 'jld-acflf')
                    );
                }

                $j++;
                array_push( $temp_locations["params"], $loc );
            }
            array_push( $locations["locations"], $temp_locations );
            $rule_groups_i++;
        }

        return $locations;
    }



    /**
    * Check if a field is required and return an html tag with a red asterisk.
    * @param  boolean  $field_required  The 0 or 1 of the required setting
    * @return string                    <span class="acf-required">*</span>
    */
    function check_required( $field_required ) {
        if( $field_required === 1 ) {
            $required_tag = ' <span class="acf-required">*</span>';
        } else {
            $required_tag = '';
        }
        return $required_tag;
    }



    /**
    * Check the return value of a field and return a formatted label.
    * @param  array  $field  Array with all the field data
    * @return string         Description of the return value
    */
    function get_image_return_value( $field ) {
        if( $field["type"] === "image" ) {
            if( $field["return_format"] === "array" ) {
                $format_label = "array";
            } else if( $field["return_format"] === "url" ) {
                $format_label = "url";
            } else if( $field["return_format"] === "id" ) {
                $format_label = "id";
            }
            $return_value = " <small>(" . $format_label . ")</small>";
        } else if( $field["type"] === "post_object" ) {
            if( $field["return_format"] === "object" ) {
                $format_label = "object";
            } else if( $field["return_format"] === "id" ) {
                $format_label = "id";
            }
            $return_value = " <small>(" . $format_label . ")</small>";
        } else if( $field["type"] === "textarea" ) {
            if( $field["new_lines"] === "wpautop" ):
                $format_label = "p";
            elseif( $field["new_lines"] === "br" ):
                $format_label = "br";
            else:
                $format_label = "plain";
            endif;
            $return_value = " <small>(" . $format_label . ")</small>";
        } else {
            $return_value = "";
        }
        return $return_value;
    }



    /**
    * Process Page ID or Post type to huan readable names
    * @param   string $param       Type of parameter
    * @param   string $param_val   Value of the parameter
    * @return  string
    */
    function process_param_value( $param, $param_val ) {

        // If Post Type
        if ( $param === __("Post type", 'jld-acflf') ) {
            $object = get_post_type_object( $param_val );
            if( $object != "" && is_object( $object ) ):
                $label = $object->labels->singular_name;
                return $label;
            endif;
        }

        // If Post status
        else if ( $param === __("Post status", 'jld-acflf') ) {
            if( $param_val != "" && is_string( $param_val ) ):
                $statuses = array(
                    'publish'		=>	__("Published", 'jld-acflf'),
                    'future'		=>	__("Future", 'jld-acflf'),
                    'draft'	        =>	__("Draft", 'jld-acflf'),
                    'pending'	    =>	__("Pendig review", 'jld-acflf'),
                    'private'		=>	__("Private", 'jld-acflf'),
                    'trash'			=>	__("Trash", 'jld-acflf'),
                    'auto-draft'	=>	__("Auto Save", 'jld-acflf'),
                    'inherit'		=>	__("Inherit", 'jld-acflf'),
                );
                return $statuses[$param_val];
            endif;
        }

        // If Category
        else if ( $param === __('Post category', 'jld-acflf') ) {
            return str_replace( 'category:', '', $param_val );
        }

        // If Taxonomy
        else if ( $param === __('Post taxonomy', 'jld-acflf') ) {
            return str_replace( 'category:', '', $param_val );
        }

        // If Taxonomy Term
        else if ( $param === __('Taxonomy Term', 'jld-acflf') ) {
            $object = get_taxonomy( $param_val );
            if( $object != '' && is_object( $object ) ):
                $label = $object->labels->name;
                return $label;
            endif;
        }

        // If Post format
        else if ( $param === __('Post format', 'jld-acflf') ) {
            // $post_formats = get_theme_support( 'post-formats' );
            return $param_val;
        }

        // If is Page, Post or Page parent
        else if( $param === __( 'Page', 'jld-acflf') || $param === __('Post', 'jld-acflf') || $param === __('Page parent', 'jld-acflf' ) ) {
            return get_the_title( $param_val );
        }

        // If Page Type
        else if ( $param === __('Page type', 'jld-acflf') ) {
            if( $param_val != '' && is_string( $param_val ) ):
                $choices = array(
                    'front_page'	=>	__('Front Page', 'jld-acflf'),
                    'posts_page'	=>	__('Posts Page', 'jld-acflf'),
                    'top_level'		=>	__('Top Level Page (no parent)', 'jld-acflf'),
                    'parent'		=>	__('Parent Page (has children)', 'jld-acflf'),
                    'child'			=>	__('Child Page (has parent)', 'jld-acflf'),
                );
                return $choices[$param_val];
            endif;
        }

        // If Options Page
        else if ( $param === __("Options page", 'jld-acflf') ) {
            if( is_array( acf_get_options_pages() ) && !empty( acf_get_options_pages() ) ):
                foreach (acf_get_options_pages() as $options_page) :
                    if ( $options_page["menu_slug"] === $param_val ) :
                        return $options_page["page_title"];
                    endif;
                endforeach;
            else:
                return __( "No options page exists. None selected.", 'jld-acflf' );
            endif;
        }

        // If User is ...
        else if ( $param === __("Current user", 'jld-acflf') ) {
            if( $param_val === "viewing_front" ):
                return __("Viewing front end", 'jld-acflf');
            elseif( $param_val === "viewing_back" ):
                return __("Viewing back end", 'jld-acflf');
            elseif( $param_val === "logged_in" ):
                return __("Logged in", 'jld-acflf');
            endif;
        }

        // Fallback
        else {
            return $param_val;
        }
    }




    /**
    * Get relationship filters
    * @param  array  $group_field ACF Group Field settings
    * @return string              Return a list of relationship filters.
    */
    function get_relations( $group_field ) {
        if( $group_field["type"] === "relationship" ) {
            $relations_post = $group_field["post_type"];
            $relations_tax = $group_field["taxonomy"];
            $total_output = " (" . $group_field["return_format"] . ")";


            if(!empty($relations_post) || !empty($relations_tax)) {
                $total_output .= "<div style='font-size: 11px; line-height: 17px;'>";
            }

            // Check for post_type relations
            if(!empty($relations_post)) {
                $relations_output = "&nbsp;&nbsp;-&nbsp;&nbsp;<strong>post type(s):</strong><br />";
                $total_relations = count($relations_post);
                $i = 1;
                foreach ($relations_post as $relation) {
                    $relations_output .= "&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;" . $relation;
                    if($total_relations != $i) {
                        $relations_output .= "<br />";
                    }
                    $i++;
                }
                $total_output .=  "<span>" . $relations_output . "</span><br />";
            }


            // Check for taxonomy relations
            if(!empty($relations_tax)) {
                $relations_tax_output = "&nbsp;&nbsp;-&nbsp;&nbsp;<strong>taxonomies:</strong><br />";
                $total_relations = count($relations_tax);
                $i = 1;
                foreach ($relations_tax as $relation) {
                    $relations_tax_output .= "&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;" . $relation;
                    if($total_relations != $i) {
                        $relations_tax_output .= "<br />";
                    }
                    $i++;
                }
                $total_output .=  "<span>" . $relations_tax_output . "</span><br />";
            }


            if(!empty($relations_post) || !empty($relations_tax)) {
                $total_output .= "</div>";
            }

            return $total_output;
        }
    }

}
