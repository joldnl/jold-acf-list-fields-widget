
var gulp          = require('gulp');
var sass          = require('gulp-sass');
var rename        = require('gulp-rename');
var uglify        = require('gulp-uglify');
var rimraf        = require('gulp-rimraf');
var runsequence   = require('run-sequence');


// Run a sequence of tasks.
gulp.task('build', function(callback) {
  runsequence(
    'clean-dist',
    'compile-sass'
    // 'compile-sass-edit-post',
    // 'compile-js'
  )
});


// Initiate watch for scss processing and js compression
gulp.task('watch', function(){
  gulp.watch('assets/scss/**/*.scss', ['compile-sass']);
  gulp.watch('assets/js/**/*.js', ['compile-js']);
})


// Clean the dist folder
gulp.task('clean-dist', function() {
  return gulp.src(['dist/js/*', 'dist/css/*'], {read: false})
         .pipe(rimraf());
})


// SCSS comilation and compression
gulp.task('compile-sass', function() {
  gulp.src('assets/scss/styles.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename('styles.min.css'))
    .pipe(gulp.dest('dist/css'));
    console.log('           SCSS (SCREEN) files compiled and CSS created!');
  gulp.src('assets/scss/styles-print.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename('styles-print.min.css'))
    .pipe(gulp.dest('dist/css'));
    console.log('           SCSS (PRINT) files compiled and CSS created!');
})


// Javascript comilation and compression
gulp.task('compile-js', function() {
  gulp.src(['assets/js/utils.js', 'assets/js/jquery.wptabs.js', 'assets/js/scripts.js'])
    .pipe(concat('scripts.min.js'))
//    .pipe(stripdebug())
//    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
    console.log('           JavaScript files compiled and created!');

});
