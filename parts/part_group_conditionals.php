<?php
    $rules = $acflf->get_group_locations( $acf_group );
    $rule_groups_i = 1;
?>

<?php foreach ( $rules['locations'] as $location ) : ?>

    <?php $j = 1; ?>
    <?php $total_groups = count( $location["params"] ); ?>

    <strong class="acflf_strong">
        <?php echo $location["label"]; ?>
    </strong>
    <ul class="conditions">

        <?php foreach ( $location["params"] as $param ) : ?>

            <li>
                <strong><?php echo $param["param"]; ?></strong>
                <?php echo $param["operator"]; ?> "<em><?php echo $acflf->process_param_value( $param["param"], $param["value"] ); ?></em>"

                <?php if( $j != $total_groups ) : ?>
                    <strong><?php echo $param["append"]; ?></strong>
                <?php endif; ?>

            </li>

            <?php $j++; ?>
        <?php endforeach; ?>

    </ul>

    <?php $rule_groups_i++; ?>

<?php endforeach; ?>
