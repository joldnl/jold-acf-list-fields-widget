<?php $acflf = new JoldAcfOverviewWidget; ?>

<?php if( !class_exists('acf') ) : ?>
    <p><?php _e( "Advanced Custom Fields Pro plugin does not seem to be active. Please <a href='plugins.php'>activate</a> Advanced Custom Fields Pro!", 'jld-acflf' ); ?></p>
<?php else: ?>

    <p><?php _e( "Display a list of all registered Advanced Custom Field Groups and their fields.", 'jld-acflf' ); ?></p>

    <table class="wp-list-table widefat fixed striped top_table">
        <thead>
            <tr>
                <th class="groups"><?php _e( "Field Groups", 'jld-acflf' ); ?></th>
                <th class="fields"><?php _e( "Custom Fields", 'jld-acflf' ); ?></th>
            </tr>
        </thead>
        <tbody>

            <?php $acf_groups = acf_get_field_groups(); ?>

            <?php if ( !empty( $acf_groups ) ) : ?>

                <?php foreach( $acf_groups as $acf_group ) : ?>
                    <tr>
                        <td class="group_details">
                            <h3>
                                <strong>
                                    <a href="post.php?post=<?php echo $acf_group["ID"]; ?>&action=edit" class="acflf_edit_link">
                                        <?php echo $acf_group['title'] . "\n"; ?>
                                        <i class="dashicons dashicons-admin-generic acf-hndle-cog acf-js-tooltip" title="<?php _e( "Edit field group", 'jld-acflf' ); ?>"></i>
                                    </a>
                                </strong>
                            </h3>
                            <?php include( ACFLF_PLUGIN_FOLDER . "/parts/part_group_conditionals.php" ); ?>
                        </td>
                        <td class="group_fields">
                            <table class="fields" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th class="field_label"><?php _e( "Label", 'jld-acflf' ); ?></th>
                                    <th class="field_type"><?php _e( "Type", 'jld-acflf' ); ?></th>
                                    <th class="field_name"><?php _e( "Name", 'jld-acflf' ); ?></th>
                                </tr>

                                <?php $group_fields = acf_get_fields( $acf_group['key'] ); ?>

                                <?php if( !empty( $group_fields ) && is_array( $group_fields ) ) : ?>

                                    <?php foreach( $group_fields as $group_field ) : ?>

                                        <tr>
                                            <td class="field_label"><?php echo $group_field["label"] . $acflf->check_required($group_field["required"]); ?></td>
                                            <td class="field_type"><?php echo $group_field["type"] . $acflf->get_image_return_value($group_field) . $acflf->get_relations($group_field); ?></td>
                                            <td class="field_name"><?php echo $group_field["name"]; ?></td>
                                        </tr>


                                        <?php if ($group_field["type"] === "flexible_content") :  // Flexible content field, loop trough sub fields ?>
                                            <?php $i = 0; ?>

                                            <tr>
                                                <td class="flexible_content" colspan="3">

                                                    <?php foreach ($group_field["layouts"] as $layout) : ?>
                                                        <table class="fields subfields" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <th class="field_label">-&nbsp;&nbsp;<?php echo $layout["label"]; ?></th>
                                                                <th class="field_type">-&nbsp;&nbsp;layout</th>
                                                                <th class="field_name">-&nbsp;&nbsp;<?php echo $layout["name"]; ?></th>
                                                            </tr>

                                                            <?php foreach ($layout["sub_fields"] as $sub_field) : ?>
                                                                <tr>
                                                                    <td class="field_label">&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $sub_field["label"] . $acflf->check_required( $sub_field["required"] ); ?></td>
                                                                    <td class="field_type">&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $sub_field["type"] . $acflf->get_image_return_value( $sub_field ) . $acflf->get_relations( $sub_field ); ?></td>
                                                                    <td class="field_name">&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $sub_field["name"]; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>

                                                        </table>
                                                    <?php endforeach; ?>

                                                </td>
                                            </tr>

                                        <?php elseif( $group_field["type"] === "repeater" || $group_field["type"] === "group" ) :  // Repeater or group field, loop trough sub-fields ?>

                                            <tr>
                                                <td class="repeater" colspan="3">
                                                    <table class="fields subfields" cellspacing="0" cellpadding="0">

                                                        <?php foreach ( $group_field["sub_fields"] as $sub_field) : ?>
                                                            <tr>
                                                                <td class="field_label">&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $sub_field["label"] . $acflf->check_required( $sub_field["required"] ); ?></td>
                                                                <td class="field_type">&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $sub_field["type"] . $acflf->get_image_return_value( $sub_field ) . $acflf->get_relations( $sub_field ); ?></td>
                                                                <td class="field_name">&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $sub_field["name"]; ?></td>
                                                            </tr>
                                                        <?php endforeach; ?>

                                                    </table>
                                                </td>
                                            </tr>

                                        <?php endif; ?>

                                    <?php endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td class="field_label no-fields" colspan="3"><?php _e( 'No fields found.', 'jld-acflf' ) ?></td>
                                    </tr>
                                <?php endif; ?>

                            </table>
                        </td>
                    </tr>

                <?php endforeach; ?>

            <?php else: ?>
                <?php _e( 'No Advanced Custom Field groups found.', 'jld-acflf' ); ?>
            <?php endif; ?>

        <tbody>
    </table>

<?php endif; ?>
